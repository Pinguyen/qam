%% Demo_qam16
% Show qam16

clc; close all; clear;

%% Modulate a Random Signal
% Generate a Random Binary Data Stream
M = 16;                     % Size of signal constellation
k = log2(M);                % Number of bits per symbol
n = 30000;                  % Number of bits to process
numSamplesPerSymbol = 1;    % Oversampling factor

rng default                 % Use default random number generator
dataIn = randi([0 1],n,1);  % Generate vector of binary data

stem(dataIn(1:40),'filled');
title('Random Bits');
xlabel('Bit Index');
ylabel('Binary Value');

% Convert the Binary Signal to an Integer-Valued Signal
dataInMatrix = reshape(dataIn,length(dataIn)/4,4);   % Reshape data into binary 4-tuples
dataSymbolsIn = bi2de(dataInMatrix);                 % Convert to integers

figure; % Create new figure window.
stem(dataSymbolsIn(1:10));
title('Random Symbols');
xlabel('Symbol Index');
ylabel('Integer Value');

% Modulate using 16-QAM
dataMod = qammod(dataSymbolsIn,M,0);         % Binary coding, phase offset = 0
dataModG = qammod(dataSymbolsIn,M,0,'gray'); % Gray coding, phase offset = 0

% Add White Gaussian Noise
EbNo = 10; % energy per bit to noise power spectral density ratio
snr = EbNo + 10*log10(k) - 10*log10(numSamplesPerSymbol);

receivedSignal = awgn(dataMod,snr,'measured');
receivedSignalG = awgn(dataModG,snr,'measured');

% Create a Constellation Diagram
sPlotFig = scatterplot(receivedSignal,1,0,'g.');
hold on
scatterplot(dataMod,1,0,'k*',sPlotFig)

% Demodulate 16-QAM
dataSymbolsOut = qamdemod(receivedSignal,M);
dataSymbolsOutG = qamdemod(receivedSignalG,M,0,'gray');

% Convert the Integer-Valued Signal to a Binary Signal
dataOutMatrix = de2bi(dataSymbolsOut,k);
dataOut = dataOutMatrix(:);                   % Return data in column vector
dataOutMatrixG = de2bi(dataSymbolsOutG,k);
dataOutG = dataOutMatrixG(:);                 % Return data in column vector

% Compute the System BER
[numErrors,ber] = biterr(dataIn,dataOut);
fprintf('\nThe binary coding bit error rate = %5.2e, based on %d errors\n', ...
    ber,numErrors)
[numErrorsG,berG] = biterr(dataIn,dataOutG);
fprintf('\nThe Gray coding bit error rate = %5.2e, based on %d errors\n', ...
    berG,numErrorsG)

%% Plot Signal Constellations
% Binary Symbol Mapping for 16-QAM Constellation

M = 16;                         % Modulation order
x = (0:15)';                    % Integer input
y1 = qammod(x, 16, 0);          % 16-QAM output, phase offset = 0

scatterplot(y1)
text(real(y1)+0.1, imag(y1), dec2bin(x))
title('16-QAM, Binary Symbol Mapping')
axis([-4 4 -4 4])

% Gray-coded Symbol Mapping for 16-QAM Constellation
y2 = qammod(x, 16, 0, 'gray');  % 16-QAM output, phase offset = 0, Gray-coded

scatterplot(y2)
text(real(y2)+0.1, imag(y2), dec2bin(x))
title('16-QAM, Gray-coded Symbol Mapping')
axis([-4 4 -4 4])

%% Pulse Shaping Using a Raised Cosine Filter
M = 16;                     % Size of signal constellation
k = log2(M);                % Number of bits per symbol
numBits = 3e5;              % Number of bits to process
numSamplesPerSymbol = 4;    % Oversampling factor

span = 10;        % Filter span in symbols
rolloff = 0.25;   % Roloff factor of filter

rrcFilter = rcosdesign(rolloff, span, numSamplesPerSymbol);

fvtool(rrcFilter,'Analysis','Impulse')

rng default                         % Use default random number generator
dataIn = randi([0 1], numBits, 1);  % Generate vector of binary data

dataInMatrix = reshape(dataIn, length(dataIn)/k, k); % Reshape data into binary 4-tuples
dataSymbolsIn = bi2de(dataInMatrix);                 % Convert to integers

dataMod = qammod(dataSymbolsIn, M);

txSignal = upfirdn(dataMod, rrcFilter, numSamplesPerSymbol, 1);

EbNo = 10;
snr = EbNo + 10*log10(k) - 10*log10(numSamplesPerSymbol);

rxSignal = awgn(txSignal, snr, 'measured');

rxFiltSignal = upfirdn(rxSignal,rrcFilter,1,numSamplesPerSymbol);   % Downsample and filter
rxFiltSignal = rxFiltSignal(span+1:end-span);                       % Account for delay

dataSymbolsOut = qamdemod(rxFiltSignal, M);

dataOutMatrix = de2bi(dataSymbolsOut,k);
dataOut = dataOutMatrix(:);                 % Return data in column vector

[numErrors, ber] = biterr(dataIn, dataOut);
fprintf('\nThe bit error rate = %5.2e, based on %d errors\n', ...
    ber, numErrors)

eyediagram(txSignal(1:2000),numSamplesPerSymbol*2);


h = scatterplot(sqrt(numSamplesPerSymbol)*...
    rxSignal(1:numSamplesPerSymbol*5e3),...
    numSamplesPerSymbol,0,'g.');
hold on;
scatterplot(rxFiltSignal(1:5e3),1,0,'kx',h);
title('Received Signal, Before and After Filtering');
legend('Before Filtering','After Filtering');
axis([-5 5 -5 5]); % Set axis ranges
hold off;

%% Error Correction using a Convolutional Code
M = 16;                                         % Size of signal constellation
k = log2(M);                                    % Number of bits per symbol
numBits = 100000;                               % Number of bits to process
numSamplesPerSymbol = 4;                        % Oversampling factor

rng default                                     % Use default random number generator
dataIn = randi([0 1], numBits, 1);              % Generate vector of binary data

tPoly = poly2trellis([5 4],[23 35 0; 0 5 13]);
codeRate = 2/3;

dataEnc = convenc(dataIn, tPoly);

dataEncMatrix = reshape(dataEnc, ...
    length(dataEnc)/k, k);                      % Reshape data into binary 4-tuples
dataSymbolsIn = bi2de(dataEncMatrix);           % Convert to integers

dataMod = qammod(dataSymbolsIn, M);

span = 10;        % Filter span in symbols
rolloff = 0.25;   % Roloff factor of filter

rrcFilter = rcosdesign(rolloff, span, numSamplesPerSymbol);

txSignal = upfirdn(dataMod, rrcFilter, numSamplesPerSymbol, 1);

EbNo = 10;
snr = EbNo + 10*log10(k*codeRate)-10*log10(numSamplesPerSymbol);

rxSignal = awgn(txSignal, snr, 'measured');

rxFiltSignal = upfirdn(rxSignal,rrcFilter,1,numSamplesPerSymbol);   % Downsample and filter
rxFiltSignal = rxFiltSignal(span+1:end-span);                       % Account for delay

dataSymbolsOut = qamdemod(rxFiltSignal, M);

dataOutMatrix = de2bi(dataSymbolsOut,k);
codedDataOut = dataOutMatrix(:);                 % Return data in column vector

traceBack = 16;                                             % Traceback length for decoding
numCodeWords = floor(length(codedDataOut)*2/3);             % Number of complete codewords
dataOut = vitdec(codedDataOut(1:numCodeWords*3/2), ...      % Decode data
    tPoly,traceBack,'cont','hard');

decDelay = 2*traceBack;                                     % Decoder delay, in bits
[numErrors, ber] = ...
    biterr(dataIn(1:end-decDelay),dataOut(decDelay+1:end));

fprintf('\nThe bit error rate = %5.2e, based on %d errors\n', ...
    ber, numErrors)